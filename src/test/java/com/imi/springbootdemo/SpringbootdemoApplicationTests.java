package com.imi.springbootdemo;

import java.util.List;

import org.junit.jupiter.api.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import com.imi.springbootdemo.service.StudentDaoService;

@SpringBootTest
class SpringbootdemoApplicationTests {
	
	private static final Logger log  = LoggerFactory
			.getLogger(SpringbootdemoApplicationTests.class);

	@Test
	void contextLoads() {
	}
	
	@Autowired
	private StudentDaoService studentService;
	
	@Test
	public void testFindStudentByLastName() {
		
		log.info("\n>>>>>> Result from testFindStudentByLastName: "
				+ studentService.findByLastName("martin"));
	}
	
	
	@Test
	public void testRetrieveAllStudentByNativeQuery() {
		
		log.info("\n>>>>>> Result from testRetrieveAllStudentByNativeQuery: "
				+ studentService.findAllStudentFromNativeQuery());
	}
	
	@Test
	public void testFindAllStudentMarksHavingBetween() {
		
		log.info("\n>>>>>> Result from testFindAllStudentMarksHavingBetween: "
				+ studentService.findStudentsForGivenScore(80, 100));
	}
	
	@Test
	public void testFindStudentByScore() {
		log.info("\n>>>>>> Result from testFindStudentByScore:\n ");
		List<Object[]> resultList = studentService.findStudentByScore(91);
		resultList.forEach(arr -> {
			log.info("LastName: " + arr[0]);
			log.info("City: " + arr[1]);
		});
	}
}
