package com.imi.springbootdemo.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.imi.springbootdemo.entity.Student;
import com.imi.springbootdemo.service.StudentDaoService;

@RestController
@RequestMapping(StudentController.BASE_URL)
public class StudentController {
	
	public static final String BASE_URL = "/api/v1/student";

	@Autowired
	private StudentDaoService studentDaoService;
	
	@GetMapping
	public List<Student> retrieveAllStudent(){
		return studentDaoService.retrieveAllStudent();
	}
	
	@GetMapping("/{id}")
	public Student findStudentById(@PathVariable Long id) {
		return studentDaoService.findStudentById(id);
	}
	
	@PostMapping
	public Student createStudent(@RequestBody Student student) {
		return studentDaoService.createStudent(student);
	}

	@DeleteMapping("/{id}")
	public void deleteStudentById(@PathVariable Long id) {
		studentDaoService.deleteStudentById(id);
	}
	
	@PutMapping
	public Student updateStudent(@RequestBody Student student) {
		return studentDaoService.updateStudent(student);
	}
}
