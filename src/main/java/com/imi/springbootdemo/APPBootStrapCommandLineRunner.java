package com.imi.springbootdemo;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.stereotype.Component;

import com.imi.springbootdemo.entity.Student;
import com.imi.springbootdemo.service.StudentDaoService;

@Component
public class APPBootStrapCommandLineRunner 
	implements CommandLineRunner {
	
	private static final Logger log = LoggerFactory
			.getLogger(APPBootStrapCommandLineRunner.class);
	
	@Autowired
	private StudentDaoService studentService;
	
	
	private void creatStudent (String fname, String lname, int score, String city) {
		
		Student student = new Student();
		student.setFirstName(fname);
		student.setLastName(lname);
		student.setScore(score);
		student.setCity(city);
		
		studentService.createStudent(student);
		log.info(">>>>>> User is created" + student);
	}

	private void generateStudents() {
		this.creatStudent("bob", "martin", 60, "hutson");
		this.creatStudent("bill", "gates", 91, "newyork");
		this.creatStudent("nitesh", "nandan", 70, "hyderabad");
		this.creatStudent("Jennifer", "jinny", 55, "london");
		this.creatStudent("john", "yusuf", 63, "paris");
	}
	@Override
	public void run(String... args) throws Exception {
//		this.generateStudents();
	}
	
	
}
