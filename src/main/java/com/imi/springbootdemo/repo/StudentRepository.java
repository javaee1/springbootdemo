package com.imi.springbootdemo.repo;

import java.util.List;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;

import com.imi.springbootdemo.entity.Student;

public interface StudentRepository extends CrudRepository<Student, Long> {
	
	// JPA Finder 
	List<Student> findByLastName(String lname);
	
	// JPQL
	@Query("from Student where score>:min and score<:max")
	List<Student> findStudentsForGivenScore(@Param("min") int min, @Param("max") int max);

	// native query
	@Query(value="select * from student", nativeQuery = true)
	List<Student> findAllStudentFromNativeQuery();
	
	// native query with limited column returned
	@Query(value="select lname, city from student where score=:score", nativeQuery = true)
	List<Object[]> findStudentByScore(@Param("score") int score);
	
	

}
