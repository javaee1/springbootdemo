package com.imi.springbootdemo.service;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Service;

import com.imi.springbootdemo.entity.Student;
import com.imi.springbootdemo.repo.StudentRepository;

@Service
public class StudentDaoService {
	
	@Autowired
	private StudentRepository studentRepository;
	
	public Student createStudent(Student student) {
		return studentRepository.save(student);
	}
	
	public Student findStudentById(Long id) {
		Optional<Student> optionalEntity = studentRepository.findById(id);
		if(optionalEntity.isPresent()) {
			return optionalEntity.get();
		}
		return null;
	}
	
	public Student updateStudent(Student student) {
		return this.createStudent(student);
	}
	
	public List<Student> retrieveAllStudent(){
		return (List<Student>) studentRepository.findAll();
	}
	
	public void deleteStudentById(Long id) {
		studentRepository.deleteById(id);
	}
	
	public List<Student> findByLastName(String lname){
		return studentRepository.findByLastName(lname);
	}
	
	
	public List<Student> findStudentsForGivenScore(int min,int max){
		return studentRepository.findStudentsForGivenScore(min, max);
	}

	public List<Student> findAllStudentFromNativeQuery(){
		return studentRepository.findAllStudentFromNativeQuery();
	}
	
	
	public List<Object[]> findStudentByScore(int score) {
		return studentRepository.findStudentByScore(score);
	}
}
