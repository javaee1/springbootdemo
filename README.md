# About the Project
This project demonstrate the basic structure and configuration of a spring boot application. Spring Boot application is best suitable for developing the microservice applicaton. <br>
Through this project we will be looking into following things:
- Advantage/Disadvantage of Application
- Spring Boot Architecute
- Spring JPA and Best Practices
- Testing

## Advantage and Disadvantage of Spring Boot Application

As we have discussed, the spring boot is best suitable for the development of microservice application. 
For benifits/disadvantage of microservice you can [click Here](.doc/MicroServiceDoc.md#why-microservice).

**Benifits of Spring Boot Application**<br>

The main goal of Spring Boot Framework is to reduce Development, Unit Test and Integration Test time and to ease the development of Production ready web applications very easily compared to existing Spring Framework, which really takes more time.

- It is very easy to develop Spring Based applications with Java.

- It avoids writing lots of boilerplate Code, Annotations and XML Configuration.

- It is very easy to integrate Spring Boot Application with its Spring Ecosystem like Spring JDBC, Spring ORM, Spring Data, Spring Security etc.

- It follows “Opinionated Defaults Configuration” Approach to reduce Developer effort(Auto Configure).

- It provides Embedded HTTP servers like Tomcat, Jetty etc. to develop and test our web applications very easily.

- It provides CLI (Command Line Interface) tool to develop and test Spring Boot Applications from command prompt very easily and quickly.

- It provides lots of plugins to develop and test Spring Boot Applications very easily using Build Tools like Maven and Gradle.

- It provides lots of plugins to work with embedded and in-memory Databases very easily.

**Disadvantages**
- Spring boot may unnecessarily increase the deployment binary size with unused dependencies.

- May you fell, Limited control of your application.

**Note:- &nbsp;** Please [click Here](.doc/MicroServiceDoc.md#general-faq.). for FAQ.

## Spring Boot Architecture

Very Basic Architecute of Spring Boot Application

![spring boot architecure](.doc/resource/springbasicArch.png)

This is the simplest architecture and it is similar to the current demo project. 

Current Project Structure

![spring boot architecure](.doc/resource/ApplicationArchitecture.jpeg)


This is very similar to famous onion architecuter.

**Controller** - It's the outer-most layer, and keeps peripheral concerns like UI and tests. For a Web application, it represents the Web API or Unit Test project. This layer has an implementation of the dependency injection principle so that the application builds a loosely coupled structure and can communicate to the internal layer via interfaces.

**Service Layer** - The Service layer holds interfaces with common operations, such as Add, Save, Edit, and Delete. Also, this layer is used to communicate between the UI layer and repository layer. The Service layer also could hold business logic for an entity. In this layer, service interfaces are kept separate from its implementation, keeping loose coupling and separation of concerns in mind.

**Repository Layer** - This layer creates an abstraction between the domain entities and business logic of an application. In this layer, we typically add interfaces that provide object saving and retrieving behavior typically by involving a database. This layer consists of the data access pattern, which is a more loosely coupled approach to data access. We also create a generic repository, and add queries to retrieve data from the source, map the data from data source to a business entity, and persist changes in the business entity to the data source.

## Spring JPA and Best Practices
We will be using Spring JPA with Hibernate. This framework
gives us enough flexiblity with query writing and pagination.
*CrudRepositry* interface all the basic crudoperation in built which makes developer task very easy.

It also support transaction management, and to configure it we just need to use one annotation (**@Transaction**) and every thing will be in place. For further reference please look the source code of this project.

**Configuring the Data Source**

```
spring.datasource.url=jdbc:mysql://localhost:3306/testdb
spring.datasource.username=root
spring.datasource.password=root
spring.jpa.show-sql=true
```

## Testing
In this project we have use Jnit. Spring Boot automatically configures the jars if it founds in classpath. For more clarification please look into source code.
