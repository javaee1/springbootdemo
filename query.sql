CREATE SCHEMA `testdb` ;

CREATE TABLE student (
    id int NOT NULL AUTO_INCREMENT,
    fname varchar(45) NOT NULL,
    lname varchar(45) NOT NULL,
    score int NOT NULL,
    city varchar(45),
    PRIMARY KEY (id)
);
