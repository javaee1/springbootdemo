

# Index
1. [Why Microservice?](#why-microservice)
2. [Which framework should be used for development and why?](#which-framework-should-be-used-for-development-and-why?)
3. [General FAQ.](#general-faq.)

## Why Microservice?
<strong>Ans:-&nbsp;</strong> Before answering this questiton first we have to understand the **flaws or limitations of Monolith** application: <br>
- **Scalability - &nbsp;** In monolith application we can only go for ***vertical*** scaling and after some point it is nearly impossible to scale further.

- **Complexity - &nbsp;** It has both development as well as testing complexity. When the application grows there is shift of focus from quality development to not breaking the things and then develop. 
In short, maintaing the application takes extra precautions and time.

- **Hard to Understand - &nbsp;** For new member, joining the team has to face a lot of challenge to understand the application, as there is no fix boundaries of services and everything bundle in one application.

- **Maintaince - &nbsp;** It takes a lot of extra effort just to maintain the application. In case of production bug, finding the root cause will take long time, as there is no logical divison of code.

- **Canary Development - &nbsp;** In Monolith, the impact of one services is huge so the canary development is nearly impossible in Monolith.

All the above issue can be easily tackled with microservices.<br>

- Horizontal Scaling
- Easy to Understand
- loosely coupled code base.
- Clear seperation of services
- Easy to Maintain and find the production bug.
- Canary Development
- Unit Testing / Proper Documentation
- Ownership of services


## Which framework should be used for development and why?
**Ans - &nbsp;** There are abundent of frameworks, we have ***php framework such as Laravel and Lumen, python framework such as djnago, node framework such as express js and most popular SpringBoot, A java framework***. <br>

Now the question is which one to choose??

Points to Consider before choosing framework:
- It should widely accepted in Industry and well tested.
- Should have a large community support.
- Easy to start (learn).
- No security breach.
- Easy to configure - Web application needs lots of configuration which might start bothering the developer.
- Easy to Maintain dependency and its version.

If we consider above things then **Springboot** comes in the first place.
It is widely used in industry and well tested framework.

# General FAQ

## Q. Advantange of Using SpringBoot for Microservice
- Highly tested and reliable enterprise services.
- Uses Java (Highly accepted and popular language in industry).
- SpringBoot uses spring core for dependency injection to make the application loosely coupled.
- It also uses spring MVC, and we can use it for highly complex application as the businees logic will be seperated from controller.

## Q. Why we are not going with the traditional framework of developing the service such as spring and spring mvc or why we don't use spring core and spring mvc rather then springboot?

<strong>Ans:- &nbsp;</strong> First of all there is no comprasion between springboot and spring mvc. Even springboot uses springmvc and spring core internally.

## Q. So what is the advantage of using springboot if it uses the same spring core and spring mvc?
<strong>Ans:- &nbsp;</strong> There are few supporting point to use it:<br>
- In traditional approach, We have to do lots of configuration for developing web application, and also we have to take care of the dependencies.
- Jar are in continuous evolving mode, somtime upgrading a jar may crash your application, the point is we have the take care even the version of jars and when the application grows it becomes very difficult to maintain.
- Springboot gives you a bundle of all the supporting jars and you don't have to bother for it versions, it will take care by itself.
- It has a vide variety of supporting jars and plugins to monitor you applications.
- It has a integrated webserver so you don't have to deploy explicitly.
- Most important it auto configures most of the things and gives us enough flexibility to change it, this helps developer to focus on the business logic/desing logic rather than scratching his head on configuration.


## Q. How we are going to access Database and how to maintain the transaction?

<strong>Ans:- &nbsp;</strong> We will be using Spring Jpa with Hibernate (ORM), It has transaction support. <br>

Advantage of Spring JPA with Hibernate
- Highly reliable
- We don't have to write the boilerplate code.
- Support of transation.
- Dirty check update

## Q. I have a concern, ORM tools like Hibernate may slow down the performance can't we go with plain JDBC where we can right native sql query?
<strong>Ans:- &nbsp;</strong> First of All, we can write native sql query whenever it requires or when we fell it might impact the performance. 
  So performance won't be a much problem here. For a moment let we go with the pure jdbc code, in tha case we have write lots of boilerplate code for connection. Writing transaction by self it not a good idea, as we are tends to make a mistake since there will be lots of duplication of code and boilerplate code, these are error prone. 
  If we use JPA properly, then we take the lots of advantage of framework without bothering the configuraiton.
